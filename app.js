var express = require('express');
var app = express();
var server = require('http').createServer(app);
var sockets = require('./routes/sockets');

var MemoryStore = express.session.MemoryStore;
var sessionStore = new MemoryStore();

// Configure app
app.configure(function () {
    app.use(express.cookieParser("secret"));
    app.use(express.session({
        store: sessionStore,
        secret: "secret"
    }));
    app.use(express.bodyParser());
    app.use(app.router);
    app.use(express.static(__dirname + '/public/static'));
	app.use(express.static(__dirname + '/public/static/pages'));
});

// Set routes after configuration is done
require('./routes')(app);

// Startup database connection
require('./models/DataAccessAdapter').InitDB();

server.listen(8080);
console.log('Listening on port 8080');

sockets.startSocketServer(server, sessionStore);