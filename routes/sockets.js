var socketio = require('socket.io');
var connect = require('express/node_modules/connect')
var parseCookie = require('express').cookieParser();
var ObjectID = require('mongodb').ObjectID;
var io, clients = {};
var utils = connect.utils;
var Session = connect.middleware.session.Session;

var _voteSession;
function getVoteSession() {
    if (!_voteSession) {
        _voteSession = require('../models/VoteSession')();
    }
    return _voteSession;
}

var userVotes = {};
var store;
var _voteSessions = {};

module.exports = {

    startSocketServer: function (server, sessionStore) {

        store = sessionStore;

        // Start listening
        io = socketio.listen(server);

        // Setup for session access
        io.set('authorization', function (data, accept) {
            // Check if there's a cookie header
            if (data.headers.cookie) {

                var splitStr = data.headers.cookie.split('=');
                var ck = utils.parseSignedCookie(unescape(data.headers.cookie), "secret");
                var sessionId = utils.parseSignedCookie(unescape(splitStr[1]), "secret");

                data.sessionStore = store;
                store.get(sessionId, function (err, session) {
                    if (err || !session) {
                        accept('Error', false);
                    }
                    else {
                        data.session = new Session(data, session);
                        data.sessionStore = store;
                        accept(null, true);
                    }
                });
            }
            else {
                return accept('No cookie transmitted.', false);
            }

            // accept the incoming connection
            accept(null, true);
        });

        io.on('connection', function (socket) {
            console.log("new connection: " + socket.id);
            var hs = socket.handshake;

            //   Chat
            // ========
            socket.on('sendchat', function (message) {
                SendChatToSession(io, _voteSessions[socket.SessionId][socket.UserId].Name, socket.SessionId, message);
            });

            // Voting
          	// ======

            socket.on('adduser', function (addUserDto) {
            	var userId = socket.userId;
                if (!socket.userId || socket.userId == '') {
                    userId = new ObjectID();
                    socket.userId = userId;

                    var userVote = {};
                    userVote.ID = userId;
                    userVote.Name = addUserDto.UserName;
                    userVote.Vote = "None";
                    userVote.socketId = socket.id;

                    AddUserVote(userVote, addUserDto.SessionId);

                    socket.UserId = userId;
                    socket.SessionId = addUserDto.SessionId;

                    // echo to client
            	    socket.emit('userAdded', userVote);

                    // Update list of users for specific session
                    UpdateSessionUsers(io, addUserDto.SessionId);
                }
            });

            socket.on('vote', function (vote) {
                if (_voteSessions && _voteSessions[socket.SessionId] && _voteSessions[socket.SessionId][socket.UserId]) {
                    _voteSessions[socket.SessionId][socket.UserId].Vote = vote;
                    UpdateSessionUsers(io, socket.SessionId);
                } 
            });

            socket.on('resetSession', function(voteSessionId) {
                ClearSessionVotes(io, voteSessionId);
            });

            socket.on('revealVotes', function(voteSessionId) {
                RevealSessionVotes(io, voteSessionId);
            });

            socket.on('disconnect', function () {
                // If user is part of vote, send message to chat.
                if (socket.UserId && socket.SessionId && _voteSessions[socket.SessionId][socket.UserId]) {
                    var name = _voteSessions[socket.SessionId][socket.UserId].Name;
                    delete _voteSessions[socket.SessionId][socket.UserId];

                    UpdateSessionUsers(io, socket.SessionId);
                }    
            });
        });
    }
};

function AddUserVote(userVote, sessionId) {
    // Add new session if it doesn't exist
    if (!_voteSessions[sessionId]){
        _voteSessions[sessionId] = {};
    }

    _voteSessions[sessionId][userVote.ID] = userVote;
}

function UpdateSessionUsers(io, voteSessionId) {
    var socketIds = GetSocketIdsForSessionId(voteSessionId);

    for (var i in socketIds) {
        io.sockets.socket(socketIds[i]).emit("updateUserVotes", _voteSessions[voteSessionId]);
    }
}

function RevealSessionVotes(io, voteSessionId) {
    var socketIds = GetSocketIdsForSessionId(voteSessionId);
    for (var i in socketIds) {
        io.sockets.socket(socketIds[i]).emit("revealVotes", _voteSessions[voteSessionId]);
    }
}

function ClearSessionVotes(io, voteSessionId) {
    for (var userId in _voteSessions[voteSessionId]) {
        _voteSessions[voteSessionId][userId].Vote = null;
    }

    var socketIds = GetSocketIdsForSessionId(voteSessionId);
    for (var i in socketIds) {
        io.sockets.socket(socketIds[i]).emit("clearSessionVotes", _voteSessions[voteSessionId]);
    }
}

function GetSocketIdsForSessionId(sessionId) {
    var socketIds = new Array();
    
    for (var userId in _voteSessions[sessionId]) {
        if (_voteSessions[sessionId][userId]) {
            if (_voteSessions[sessionId][userId].socketId) {
                var socketId = _voteSessions[sessionId][userId].socketId;
                socketIds.push(socketId);
            }
        }
    }

    return socketIds;
}

function SendChatToSession(io, userName, sessionId, message) {
    var socketIds = GetSocketIdsForSessionId(sessionId);
    
    for (var i in socketIds) {
        io.sockets.socket(socketIds[i]).emit("updateChat", userName, message);
    }
}

