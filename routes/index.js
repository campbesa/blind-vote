
var VoteSession = require('../models/VoteSession');

module.exports = function (app) {

    app.get('/', function (req, res) {
        res.redirect('CreateOrJoin.html');
    });
    
    app.post("/Sessions/VoteSession", function (req, res) {
    	// Initialize vote session to save user stories
        var voteSession = new VoteSession();
        voteSession.SessionName = req.param("SessionName");
        voteSession.VotingOptions = req.param("VotingOptions");
        voteSession.ID = req.param("ID");
        voteSession.UserName = req.param("UserName");
             
        voteSession.Save(voteSession.ID, function (voteSessionId, message) {
        	if (message) {
                var response = {
                	Result: false,
                    Message: message
                };
                res.send(310, JSON.stringify(response));
            }
        	else {
              	var response = {
              		Result: true,
              		ID: voteSessionId
              	};
              	res.send(200, JSON.stringify(response));
        	}
     	});
	});

    // Get sessions from DB for user.
    app.get("/Sessions/VoteSessionList", function (req, res) {

        var voteSession = new VoteSession();

        voteSession.GetVoteSessions(function(err, sessions) {
        	if (err) {
        		ReturnError(res, err);
        	}
            else {                
            	var response = {
                	Result: true,
                    Sessions: sessions
                }
                res.send(200, JSON.stringify(response));
            }
		});
    });
    
    // Delete session from DB
    app.del("/Sessions/VoteSession", function (req, res) {
        var voteSessionId = req.query.voteSessionId;

        var voteSession = new VoteSession();
        voteSession.Delete(voteSessionId, function (count, err) {
            if (err) {
                response = {
                    Result: false,
                    Message: err
                };
                res.send(310, JSON.stringify(response));
            }
            else {
                response = {
                    Result: true
                }
                res.send(200, JSON.stringify(response));
            }
        });
    });

    // Get sessions from DB for user.
    app.get("/Sessions/VoteSession", function (req, res) {
        var voteSessionId = req.query.voteSessionId;
        var voteSession = new VoteSession();
        var response;

        voteSession.Load(voteSessionId, function (voteSession, err) {
            if (!voteSession || err) {
                response = {
                    Result: false,
                    Message: err
                };
                res.send(310, JSON.stringify(response));
            }
            else {
                response = {
                    Result: true,
                    VoteSession: voteSession,
                    IsAdmin: false
                }

                res.send(200, JSON.stringify(response));
            }
        });
    });
}

function ReturnError(res, message) {
    var response = {
        Result: false,
        Message: message
    };
    res.send(310, JSON.stringify(response));
}