﻿var mongo = require('mongodb');
var Server = mongo.Server;
var Db = mongo.Db;
var server_config = new Server('localhost', 27017, { auto_reconnect: true });
var db;

var DAL = function(){
    this.Connected = false;
}

DAL.prototype.Connect = function(callbackFn){
    
    this.DB = new Db('BlindVoteDB', server);

    this.DB.open(function(err, db) {
      if(!err) {
        console.log("We are connected");
        this.Connected = true;
        if (callbackFn){
            callbackFn();
        }
      }
      else{
        console.log("Error connecting to DB: " + err);
        this.Connected = false;
      }
    });
}

DAL.prototype.SaveUserStories = function(sessionName, userStories){
    console.log('Begin SaveUserStories');
    
    var server = new Server('localhost', 27017, {auto_reconnect: true});
    this.DB = new Db('BlindVoteDB', server);

    this.DB.open(function (err, db) {
        if (typeof db !== 'undefined' && db !== null){
            db.createCollection('UserStories', function(err, collection) {
                if (err){
                    console.log(err);
                }
                else
                {
                    for (story in userStories){
                        var userStory = { 'SessionName' : sessionName, 'sessionId' : 'test', 'StoryId' : story, 'StoryText' : userStories[story] };
                        collection.insert(userStory, {safe:true}, function(err, result) {
                            if (err){
                                console.log(err);
                            }
                            else{
                                console.log('Save successful');
                            }
                        });
                    }
                }
            });
        }
        else{
            console.log('DAL DB undefined.');
        }
    });
    
    console.log('End SaveUserStories');
}

/// Saves voting options
/// session - Session object
/// voting options - Array of VotingOption objects
DAL.prototype.SaveVotingOptions = function(session, votingOptions){

}

/* Exports */
module.exports = DAL;