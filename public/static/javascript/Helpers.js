﻿// jQuery add-ons

(function ($) {
    $.QueryString = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))

    $.HostName = (function () {
        var l = document.createElement("a");
        l.href = document.URL;
        return l.hostname;
    })()
})(jQuery);

// Lazy load socket connection
var _socket = null;
function Socket() {
    if (_socket === null) {
        _socket = io.connect($.HostName);
    }
    return _socket;
}

// User Story request object
    var SessionUserStories = function(){
        var sessionName;
        var userStories;
        
        function AddUserStoy(storyId, story)
        {
            userStories.push(story);
        }
        
        return{
            SessionName : sessionName,
            UserStories : userStories,
            
            AddUserStory: function(storyId, story){
                AddUserStoy(storyId, story);
            }
        };
    }