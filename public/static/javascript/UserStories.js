﻿var UserStories = exports.UserStories = function(sessionName, stories){
    this.SessionName = sessionName || "";
    
    if (stories === undefined){
        stories = [];
    }
    
    this.UserStories = stories
};

UserStories.prototype.AddUserStory = function(storyId, story)
{
    this.UserStories.push(story);
}

UserStories.prototype.Save = function(dal)
{
    console.log('BL save called');
    
    if (typeof dal === 'undefined'){
        console.log('Dal not provided for save function');
    }
    
    //dal.Connect();
    dal.SaveUserStories(this.SessionName, this.UserStories);
}
