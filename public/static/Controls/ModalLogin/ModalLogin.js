
// ModalLogin Class

var ModalLogin = function() {
    
    var loginboxHTML = '<div id="login-box" class="login-popup">' +
    	'<a href="#" class="close">' +
    		'<img src="/Controls/ModalLogin/close_pop.png" class="btn_close" title="Close Window" alt="Close" />' +
    	'</a>' +
    	'<h1 class="login-header">Please Enter a Display Name...</h1>' +
    	'<input id="username" name="username" value="" type="text" autocomplete="on" placeholder="Display Name">' +
    	'<input id="btnOK" class="ok-button" type="button" value="OK"></button>' +
    	'<br />' +
        '<br />' +
    '</div>';
    
    var _callbackMethod;
    
    function GetUserName(callback) {

        // Add HTML components if not already there.
		InitializeHtmlComponents();
        
        var loginBox = $('#login-box');
        CenterLoginBox(loginBox);
        loginBox.fadeIn(300);
        
        var mask = $('#mask');
        mask.fadeIn(300);
        
        $('#username').focus();

        _callbackMethod = callback;
    }
    
    function InitializeHtmlComponents() {
        if ($('#login-box').length == 0) {
            $('body').append(loginboxHTML);
            AddLoginEvents();
        }
        
        if ($('#mask').length == 0) {
            $('body').append('<div id="mask"></div>');
            AddMaskEvents();
        }
    }
    
    function AddLoginEvents() {
        $('#btnOK').click(function() {
                        var userName = $('#username').val();
                          if (userName && userName != "") {
                        	ClearModalLogin();
                            _callbackMethod(userName);
                          }
                        });
        $('#username').keypress(function(event) {
            if ( event.which == 13 ) {
                $('#btnOK').click();
    		}
        });
    }
    
    function AddMaskEvents() {
        // When clicking on the button close or the mask layer the popup closed
        $('a.close, #mask').on('click', function() {
                                 	ClearModalLogin();
                                 });
    }
    
    function ClearModalLogin() {
        $('#mask , .login-popup').fadeOut(300 , function() {
                                          $('#mask').remove();
                                          });
    }
    
    function CenterLoginBox(loginBox) {
        //Set the center alignment padding + border
      	var popMargTop = (loginBox.height() + 24) / 2;
      	var popMargLeft = (loginBox.width() + 24) / 2;
        
      	loginBox.css({
                     'margin-top' : -popMargTop,
                     'margin-left' : -popMargLeft
                     });
    }
    
    
    return {
        GetUserName : GetUserName
    };
}