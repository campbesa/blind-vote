# Blind Vote #
Version 1.0

### A poker planning application for distributed teams ###

A simplistic take on poker planning, emphasizing on fast, real-time multi-user interaction. This is a tool to supplement local or distributed poker planning meetings. Anyone can create a session with customizable voting options and within a few minutes, a team can start voting. Users can't see each others' votes until all votes have been cast. If a user has fallen asleep or left the room, a scrum master can reveal all the votes without having to wait for the slacker.  
  
This application is written for node.js for its simple and fast setup time and its easy-to-use websocket functionality for server-browser full duplex communication. Data is persisted in a lightweight MongoDB instance.

### How do I get set up? ###

Note: These instructions are directed towards Windows, but both node.js and MongoDB support other platforms, like Linux and OS X, so this can run on a Linux or OS X as well.

#### node.js ####
Download and install node.js
   http://nodejs.org/download/

#### MongoDB ####
* Download MongoDB
   http://www.mongodb.org/downloads
* Install MongoDB. I find it easiest to do it as a Windows Service so it starts up automatically. Here's a link on how to do that:
http://docs.mongodb.org/manual/tutorial/install-mongodb-on-windows/#configure-a-windows-service-for-mongodb
* Here's an example command... One that I've used to create a mongo database quickly without a config file. I put the database files on a second hard drive.  
	
```
#!shell

C:\Program Files (x86)\MongoDB>bin\mongod.exe --dbpath="E:\MongoDB\data\db" --logpath="C:\Program Files (x86)\MongoDB\log\mongo.log"  --install
```


### Running the Application ###

* Get the files from the repository
* Open up the project directory where app.js is located
* Run node's package manager to install package dependencies, like this:
	
```
#!shell

npm install
```

*Troubleshooting Note: I ran into a problem with the latest version of node using Windows Server 2008 R2, where I had to create a folder called "npm" in C:\Users\Administrator\AppData\Roaming\ in order to get npm to complete successfully.*

* Run the application. By default, it runs on port 8080

```
#!shell

node app.js
```



### Contribution guidelines ###

* Try not to break it.


### Who do I talk to? ###
Sam: sjcampbell40@gmail.com