// VoteSession Class

var dataAccessAdapter = require('./DataAccessAdapter');

function VoteSession(name, votingOptions) {
    VoteSession.SessionName = name || "";

    VoteSession.VotingOptions = votingOptions || [];

    console.log('getting votesession db');
    if (!VoteSession.db) {
	    console.log('initializing vote setssion db');
        VoteSession.db = dataAccessAdapter.GetDB();
    }
    if (!VoteSession.VoteCollection) {
        console.log('initializing vote session collection');
        VoteSession.VoteCollection = VoteSession.db.collection('VoteSession');
    }

    return VoteSession;
}

module.exports = VoteSession;

VoteSession.ID = null;

VoteSession.SessionName = null;

VoteSession.VotingOptions = [];

VoteSession.Load = function (voteSessionId, callback) {
    var o_id = dataAccessAdapter.BsonIdFromString(voteSessionId);
    VoteSession.VoteCollection.findOne({ '_id': o_id }, function (err, document) {

        if (!document) {
            var msg = "Vote session could not be found in the database.";
            console.warn(msg);
            callback(null, msg);
        }
        else {
            callback(document, "");
        }
    });
}

VoteSession.Save = function (voteSessionId, callback) {
    
    // If session is new, make sure the name isn't taken
    VoteSession.VoteCollection.findOne({ "SessionName": VoteSession.SessionName }, function (err, document) {

        // If a document with that name exists and the doc ID is not equal to the one passed in, don't allow it.
        if (document && document._id != voteSessionId) {
            var errMsg = "Session name is already in use.";
            console.warn(errMsg);
            callback("", errMsg);
           	return;
        }
                                       
        var document = GetDocumentForVoteSession();

        // If id is empty, save it as a new session.
        if (!voteSessionId) {
           	InsertNewVoteSession(document, callback);
        }
        else {
        	UpdateVoteSession(voteSessionId, document, callback);
        }
    });
}

VoteSession.GetVoteSessions = function (callback) {
    VoteSession.VoteCollection.find({}, function (err, documents) {
    	documents.toArray(function (err, documents) {
        	callback(err, documents);
        });
    });
}

VoteSession.Delete = function (voteSessionId, callback) {
    var bsonId = dataAccessAdapter.BsonIdFromString(voteSessionId);

    VoteSession.VoteCollection.remove({ "_id": bsonId }, { w:1 }, function (err, numRemoved) {
        if (err) {
            console.warn(err.message);
            callback("", err.message);
        }
        else {
            console.log('successfully deleted session');
            callback(numRemoved);
        }
    });
}

function GetDocumentForVoteSession(voteSession) {
    var document = {
	    SessionName: VoteSession.SessionName,
    	VotingOptions: []
    };
    
    for (var k in VoteSession.VotingOptions) {
        var option = VoteSession.VotingOptions[k];
        document.VotingOptions.push(option);
    }
    
    return document;
}

function InsertNewVoteSession(document, callback) {
    VoteSession.VoteCollection.insert(document, { safe: true }, function (err, result) {
    	if (err) {
        	console.warn(err.message);
            callback("", err.message);
        }
        else {
        	console.log('successfully inserted session');
            callback(document._id.toString());
        }
    });
}

function UpdateVoteSession(voteSessionId, document, callback) {
    var o_id = dataAccessAdapter.BsonIdFromString(voteSessionId);
    VoteSession.VoteCollection.update({ '_id': o_id }, document, { safe: true }, function (err, data, doc) {
        if (err) {
            console.warn(err.message);
            callback("", err.message);
        }
        else {
            console.log('successfully updated session');
            callback(voteSessionId);
        }
	});
}

